//
//  SCSAppDelegate.h
//  Shepherd
//
//  Created by Benjamin Craig on 4/22/14.
//  Copyright (c) 2014 Slalom Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
