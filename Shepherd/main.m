//
//  main.m
//  Shepherd
//
//  Created by Benjamin Craig on 4/22/14.
//  Copyright (c) 2014 Slalom Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SCSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SCSAppDelegate class]));
    }
}
