//
//  SCSViewController.m
//  Shepherd
//
//  Created by Benjamin Craig on 4/22/14.
//  Copyright (c) 2014 Slalom Consulting. All rights reserved.
//

#import "SCSViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>


@interface SCSViewController () <CBPeripheralManagerDelegate, CLLocationManagerDelegate>
@property (strong, nonatomic) IBOutlet UISwitch *leaderSwitch;
@property (strong, nonatomic) IBOutlet UILabel *statusText;
@property (strong, nonatomic) CLBeaconRegion *myBeaconRegion;
@property (strong, nonatomic) NSDictionary *myBeaconData;
@property (strong, nonatomic) CBPeripheralManager *peripheralManager;
@property (strong, nonatomic) IBOutlet UILabel *statusLabel;
@property (strong, nonatomic) IBOutlet UILabel *distanceLabel;
@property (strong, nonatomic) IBOutlet UILabel *distanceValueLabel;

// Receiver
@property (strong, nonatomic) CLLocationManager *locationManager;

@end

@implementation SCSViewController
static NSString *UUID = @"836208EB-69CA-4C74-82CF-62F44E8DD7E6";


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self.leaderSwitch setOn:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)leaderChanged:(id)sender {
    if([self.leaderSwitch isOn]) {
        [self.statusText setText:@"Leader"];
        [self startLeaderBroadcast];
        [self stopReceiver];
        [self hideFollowerLabels];
    }
    else {
        [self.statusText setText:@"Follower"];
        [self stopLeaderBroadcast];
        [self startReceiver];
        [self showFollowerLabels];
    }
}

// Broadcast part

-(void)startLeaderBroadcast {
    // Create a NSUUID object
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:UUID];

    // Initialize the Beacon Region
    self.myBeaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid
                                                                  major:1
                                                                  minor:1
                                                             identifier:@"com.slalom.shepherd"];
    
    // Get the beacon data to advertise
    self.myBeaconData = [self.myBeaconRegion peripheralDataWithMeasuredPower:nil];
    
    // Start the peripheral manager
    self.peripheralManager = [[CBPeripheralManager alloc] initWithDelegate:self
                                                                     queue:nil
                                                                   options:nil];

}

-(void)stopLeaderBroadcast {
    
    self.myBeaconData = nil;
    self.myBeaconRegion = nil;
    self.peripheralManager = nil;
    
}

-(void)peripheralManagerDidUpdateState:(CBPeripheralManager*)peripheral
{
    if (peripheral.state == CBPeripheralManagerStatePoweredOn)
    {
        // Bluetooth is on
        
        // Update our status label
        self.statusLabel.text = @"Broadcasting...";
        
        // Start broadcasting
        [self.peripheralManager startAdvertising:self.myBeaconData];
    }
    else if (peripheral.state == CBPeripheralManagerStatePoweredOff)
    {
        // Update our status label
        self.statusLabel.text = @"Stopped";
        
        // Bluetooth isn't on. Stop broadcasting
        [self.peripheralManager stopAdvertising];
    }
    else if (peripheral.state == CBPeripheralManagerStateUnsupported)
    {
        self.statusLabel.text = @"Unsupported";
    }

    
    [self showNotification:self.statusLabel.text];
}

-(void) showNotification:(NSString*)notificationText{
    // Get the current date
    NSDate *pickerDate = [NSDate date];
    
    // Schedule the notification
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = pickerDate;
    localNotification.alertBody = notificationText;
    localNotification.alertAction = @"Show me the item";
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
    // Request to reload table view data
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadData" object:self];
}

-(void) hideFollowerLabels
{
    self.distanceLabel.hidden=YES;
    self.distanceValueLabel.hidden=YES;
    self.statusLabel.hidden = NO;
}


-(void) showFollowerLabels
{
    self.distanceLabel.hidden=NO;
    self.distanceValueLabel.hidden=NO;
    self.statusLabel.hidden=YES;
}


// Receiver part
- (void)startReceiver
{
    // Initialize location manager and set ourselves as the delegate
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    // Create a NSUUID with the same UUID as the broadcasting beacon
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:@"A77A1B68-49A7-4DBF-914C-760D07FBB87B"];
    
    // Setup a new region with that UUID and same identifier as the broadcasting beacon
    self.myBeaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid
                                                             identifier:@"com.appcoda.testregion"];
    
    // Tell location manager to start monitoring for the beacon region
    [self.locationManager startMonitoringForRegion:self.myBeaconRegion];
}

- (void)stopReceiver
{
    
}

- (void)locationManager:(CLLocationManager*)manager didEnterRegion:(CLRegion*)region
{
    [self.locationManager startRangingBeaconsInRegion:self.myBeaconRegion];
    //self.statusText.text = @"enter region";

}

-(void)locationManager:(CLLocationManager*)manager didExitRegion:(CLRegion*)region
{
    [self.locationManager stopRangingBeaconsInRegion:self.myBeaconRegion];
    self.statusText.text = @"exit region";
}

-(void)locationManager:(CLLocationManager*)manager
       didRangeBeacons:(NSArray*)beacons
              inRegion:(CLBeaconRegion*)region
{
    // Beacon found!
    self.statusLabel.text = @"Beacon found!";
    
    CLBeacon *foundBeacon = [beacons firstObject];
    
    // You can retrieve the beacon data from its properties
    //NSString *uuid = foundBeacon.proximityUUID.UUIDString;
    //NSString *major = [NSString stringWithFormat:@"%@", foundBeacon.major];
    //NSString *minor = [NSString stringWithFormat:@"%@", foundBeacon.minor];
}

@end
